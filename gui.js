console.log(iterations);
const possiblePointsNames = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

const iterationsElement = document.getElementById("iterations");

iterationsElement.addEventListener("input", (event) => {
    iterations = document.getElementById("iterations").value;
})

const populationSizeElement = document.getElementById("populationSize");

populationSizeElement.addEventListener("input", (event) => {
    populationSize = document.getElementById("populationSize").value;
})

const mutateTimesElement = document.getElementById("mutateTimes");

mutateTimesElement.addEventListener("input", (event) => {
    mutateTimes = document.getElementById("mutateTimes").value;
})

const crossoverTimesElement = document.getElementById("crossoverTimes");

crossoverTimesElement.addEventListener("input", (event) => {
    crossoverTimes = document.getElementById("crossoverTimes").value;
})

const startingPointElement = document.getElementById("startingPoint");

startingPointElement.addEventListener("input", (event) => {
    startingPoint = document.getElementById("startingPoint").value;
})

drawPoints();

const calculateElement = document.getElementById("calculate");

calculateElement.addEventListener("click", (event) => {
    const outcome = calculate()

    outcome.individual.unshift(startingPoint)
    outcome.individual.push(startingPoint)
    const path = outcome.individual.join(" - ");
    const outcomeElement = document.getElementById("outcome");
    outcomeElement.innerText = `Ścieżka: ${path.toUpperCase()} odległość: ${outcome.individualDistance}`
})

const addPointButton = document.getElementById("addPoint");

addPointButton.addEventListener("click", (event) => {
    const pointXElement = document.getElementById("pointX");
    const pointYElement = document.getElementById("pointY");
    const pointX = Number(pointXElement.value);
    const pointY = Number(pointYElement.value);
    const pointNames = Object.keys(points);
    const lastPointNae = pointNames.pop();
    const nextName = possiblePointsNames[possiblePointsNames.indexOf(lastPointNae) + 1];
    const newPoint = { x: pointX, y: pointY };
    points[nextName] = newPoint
    drawPoint(nextName, newPoint);
})

function drawPoints() {
    const pointNames = Object.keys(points);
    for (let i = 0; i < pointNames.length; i++) {
        const pointName = pointNames[i];
        const point = points[pointName];
        drawPoint(pointName, point);
    }
}

function drawPoint(pointName, point) {
    const calculateElement = document.getElementById("calculate");
    const newDiv = document.createElement("div");
    const className = `point_${pointName}`;
    const crossSpan = `<span id=${className} class='${className}' name= ${pointName}>X</span>`;
    newDiv.innerHTML = `Punkt ${pointName.toLocaleUpperCase()} (${point.x}, ${point.y}) ${crossSpan}`;
    newDiv.classList.add(className);
    document.body.insertBefore(newDiv, calculateElement);
    const xSpan = document.getElementById(className);
    xSpan.addEventListener("click", (event) => {
        const deletedPointName = event.target.id.split("_")[1];
        const deletedPointClass = event.target.id;
        delete points[deletedPointName];
        const deletedPointELements = document.getElementsByClassName(deletedPointClass);
        for (let i = 0; i < deletedPointELements.length; i++) {
            deletedPointELements.item(i).remove();
        }
    })
}
