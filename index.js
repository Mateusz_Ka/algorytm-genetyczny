let points = {
    a: { x: 0, y: 0 },
    b: { x: 1, y: 1 },
    c: { x: 1, y: 0 },
    d: { x: 1, y: 8 },
    e: { x: 5, y: 5 },
    f: { x: 10, y: 0 },
};
let startingPoint = "a";
let iterations = 4;
let populationSize = 6;
let mutateTimes = 1;
let crossoverTimes = 2;


const distance = (p1, p2) => Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));

const randomOrder = arr => arr.map(n => ({ n, weight: Math.random() }))
    .sort((a, b) => a.weight - b.weight)
    .map(n => n.n);

const pickRandomIndex = arr => Math.floor(Math.random() * arr.length)
const pickRandomElement = arr => arr[pickRandomIndex(arr)]

function calculate() {
    let pointsNames = Object.keys(points).filter(p => p !== startingPoint);
    let population = createFirstPopulation(pointsNames);
    console.log("\n----Początkowa populacja----\n")
    console.log(population);

    for (let i = 0; i < iterations; i++) {
        console.log("\n----Operacje na populacji----\n")
        crossover(population);
        mutate(population)
        population = population.sort((a, b) => a.individualDistance - b.individualDistance).slice(0, populationSize);
        console.log("\n----Kolejna populacja----\n")
        console.log(population);
    }

    const outcome = population[0];
    console.log("\n----Wynik----\n")
    console.log(outcome);
    return outcome;
}

function createFirstPopulation(pointsNames) {
    const population = [];
    for (let i = 0; i < populationSize; i++) {
        const individual = randomOrder(pointsNames);
        const individualDistance = distanceForIndividual(individual);
        population.push({ individual, individualDistance })
    }
    return population;
}

function distanceForIndividual(a) {
    let maxDistance = distance(points[startingPoint], points[a[0]]);
    for (let j = 1; j < a.length; j++) {
        maxDistance += distance(points[a[j - 1]], points[a[j]]);
    }
    maxDistance += distance(points[a[a.length - 1]], points[startingPoint]);
    return maxDistance;
}

function crossover(population) {
    const crossedoverIndividuals = [];
    for (let i = 0; i < crossoverTimes; i++) {
        const firstIndividual = pickRandomElement(population).individual;
        let secondIndividual = pickRandomElement(population).individual;
        while (firstIndividual === secondIndividual) {
            secondIndividual = pickRandomElement(population).individual
        }
        const newIndividual = crossoverIndividuals(firstIndividual, secondIndividual);
        const newElement = {
            individual: newIndividual,
            individualDistance: distanceForIndividual(newIndividual)
        }
        console.log(`Krzyżowanie nr ${i + 1}`, firstIndividual, secondIndividual, newElement);
        crossedoverIndividuals.push(newElement);
    }

    population.push(...crossedoverIndividuals);
}

function crossoverIndividuals(firstIndividual, secondIndividual) {
    const index = Math.floor(firstIndividual.length / 2);
    const newIndividual = firstIndividual.slice(0, index);
    return newIndividual.concat(
        firstIndividual.slice(index)
            .sort((a, b) => secondIndividual.indexOf(a) - secondIndividual.indexOf(b))
    );
}

function mutate(population) {
    for (let i = 0; i < mutateTimes; i++) {
        const randomElement = pickRandomElement(population);
        const randomIndividual = randomElement.individual;
        console.log("mutacja", randomIndividual);
        const firstRandomIndex = pickRandomIndex(randomIndividual);
        let secondRandomIndex = pickRandomIndex(randomIndividual);
        while (firstRandomIndex === secondRandomIndex) {
            secondRandomIndex = pickRandomIndex(randomIndividual);
        }
        let n1 = randomIndividual[firstRandomIndex];
        let n2 = randomIndividual[secondRandomIndex];
        console.log("zamieniane elementy", n1, n2)
        let temp = n1;
        n1 = n2;
        n2 = temp;

        randomIndividual[firstRandomIndex] = n1;
        randomIndividual[secondRandomIndex] = n2;

        randomElement.individualDistance = distanceForIndividual(randomIndividual)
        console.log("wynik mutacji", randomElement)
    }
}
